## 1. GitHub homepage

- go to the front page of your GitHub homepage
- print the front page as a PDF file

## 2. GitHub profile

- go to the profile page of your GitHub account at `Your profile` in the drop-down menu from the top-right profile image
- print the profile page as a PDF file

## 3. GitHub repository for the assignment

- go to the repository for the assignments at your GitHub
- the repository must be private
- print the repository page as a PDF file

## 4. Collaborators for the assiggnment repository at GitHub

- go to the `Collaborators` page under the `Settings` menu of the assignment repository
- add the following GitHub accounts (Tak-jae-ho, sihan827, seonghoonham97) to the collaborators
- print the collaborator page as a PDF file

## 5. Run the baseline code at Google Colab 

- create an account at [Google Colaboratory](https://colab.research.google.com)
- download the baseline jupyter notebook file [assignment_01.ipynb](https://gitlab.com/cau-class/neural-network/2022-2/assignment/-/blob/master/01/assignment_01.ipynb) and open the notebook at your Google Colab
- connect your GitHub repository for the assignment to your Google Colaboratory (or your local drive with Visual Studio Code)
- see [assignment_01_colab_github_demo.ipynb](https://gitlab.com/cau-class/neural-network/2022-2/assignment/-/blob/master/01/assignment_01_colab_github_demo.ipynb)
- run all the cells in the notebook at your Google Colab 
- export the notebook after running all the cells to a PDF file at your Google Colab (or Visual Studio Code)
- see [this blog](https://velog.io/@s6820w/colab3) in order to avoid errors in exporting figures to a PDF file
- note that you can use [Visual Studio Code](https://code.visualstudio.com) instead of Google Colaboratory

## 6. Add, Commit and Push the baseline code at Google Colab

- make `git add` the baseline notebook file to the repository for the assignment at your GitHub
- make `git commit -m "initial commit"` before running the notebook
- make `git commit -m "final commit"` after running all the cells in the notebook
- make `git push` the notebook to the repository for the assignment at your GitHub
- export the GitHub history page for the notebook to a `PDF` file
- note that you can use [Visual Studio Code](https://code.visualstudio.com) instead of Google Colaboratory

## 7. Create a notebook file and write mathematical expressions in LaTeX 

- Create a notebook at Google Colaboratory
- Add a text cell to the created notebook
- Write the following mathematical expressions using LaTeX 
    - (1) $` f(x) = a x^2 + b x + c `$
    - (2) $` f(x) = \exp(x) `$
    - (3) $` f(x) = \frac{a}{b}, \quad a \in \mathbb{R}, b \in \mathbb{R} `$
    - (4) $` f(x) = \sum_{i = 1}^{n} x_i, \quad x = (x_1, x_2, \cdots, x_n) `$
    - (5) $` f(x) = \int_{\Omega} \sigma(x, t) dt `$
    - (6) $` \frac{\partial \mathbb{E}}{\partial w} `$
    - (7) $` w^{t+1} = w^{t} - \eta \nabla \mathcal{L}(w^{t}) `$
- export the notebook to a PDF file
- note that you can use [Visual Studio Code](https://code.visualstudio.com) instead of Google Colaboratory

---

# [Submission]

1. [x] PDF file for the front page of your GitHub homepage (filename should be `01-whatever-you-like.pdf`)
1. [x] PDF file for the profile of your GitHub account (filename should be `02-whatever-you-like.pdf`)
1. [x] PDF file for your GitHub repository of the assignment (filename should be `03-whatever-you-like.pdf`)
1. [x] PDF file for the collaborators of your GitHub repository for the assignment (filename should be `04-whatever-you-like.pdf`)
1. [x] PDF file for the complete baseline notebook (filename should be `05-whatever-you-like.pdf`)
1. [x] PDF file for the GitHub history of the baseline notebook file (filename should be `06-whatever-you-like.pdf`)
1. [x] PDF file for the notebook of the mathematical expressions in LaTeX (filename should be `07-whatever-you-like.pdf`)


 

